import { withTranslation } from "react-i18next";
import { useSelector } from "react-redux";
import PropTypes from "prop-types";
import { authSelectors } from "../redux/auth";
import { getTheme } from "../redux/theme/theme-selectors";
import Feedback from '../components/Feedback';
import styles from "./HomeView.module.css";


const HomeView = ({ t }) => {
  const themeLight = useSelector(getTheme);
  const isAuthenticated = useSelector(authSelectors.getIsAuthenticated);
  const name = useSelector(authSelectors.getUsersName);

  return (
    <div className={styles.container}>
      {isAuthenticated ? (
        <h2 className={themeLight ? styles.titleLight : styles.titleDark}>
          {t("title")},{name}!
        </h2>
      ) : (
        <h2 className={themeLight ? styles.titleLight : styles.titleDark}>
          {t("title")}!
        </h2>
      )}
      <p
        className={
          themeLight ? styles.descriptionLight : styles.descriptionDark
        }
      >
        {t("description")}
      </p>
      {isAuthenticated && (
        <>
          <p
            className={
              themeLight ? styles.descriptionLight : styles.descriptionDark
            }
          >
            {t("feedback")}
          </p>
          <Feedback/>
        </>
      )}
    </div>
  );
};

HomeView.propTypes = {
  t: PropTypes.func,
};

export default withTranslation()(HomeView);