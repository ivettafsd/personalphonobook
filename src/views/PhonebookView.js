import { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { withTranslation } from "react-i18next";
import PropTypes from "prop-types";
import {
  phonebookOperations,
  phonebookSelectors,
} from "../redux/phonebook/";
import { getTheme } from "../redux/theme/theme-selectors";
import ContactForm from "../components/ContactForm";
import Filter from "../components/Filter";
import ContactList from "../components/ContactList";
import styles from "./PhonebookView.module.css";

function PhonebookView({ t }) {
  //mapStateToProps
  const themeLight = useSelector(getTheme);
  const isLoadingContacts = useSelector(phonebookSelectors.getLoading);

  //mapDispatchToProps
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(phonebookOperations.fetchContacts())
  }, [dispatch])

  return (
    <div className={styles.container}>
        <ContactForm />
        <h2 className={themeLight ? styles.titleLight : styles.titleDark}>
          {t("subtitle")}
        </h2>
        <Filter />
        {isLoadingContacts && (
          <h2 className={themeLight ? styles.titleLight : styles.titleDark}>
            {t("loading")}
          </h2>
        )}
        <ContactList />
    </div >
    );
}

PhonebookView.propTypes = {
  t: PropTypes.func,
};

export default withTranslation()(PhonebookView);