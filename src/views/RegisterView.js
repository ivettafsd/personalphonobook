import { useState, useCallback } from "react";
import { useSelector, useDispatch } from "react-redux";
import { withTranslation } from "react-i18next";
import PropTypes from "prop-types";
import { getTheme } from "../redux/theme/theme-selectors";
import { authOperations, authSelectors } from "../redux/auth";
import styles from "./RegisterView.module.css";

function RegisterView({ t }) {
  //mapStateToProps
  const themeLight = useSelector(getTheme);
  const registrationError = useSelector(authSelectors.getErrorMessage);

  //mapDispatchToProps
  const dispatch = useDispatch();

  //state
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const handleChange = useCallback((event) => {
    const { name, value } = event.currentTarget;

    switch (name) {
      case "name":
        return setName(value);

      case "email":
        return setEmail(value);

      case "password":
        return setPassword(value);

      default:
        return null;
    }
  }, [])

  const handleSubmit = useCallback((event) => {
    event.preventDefault();
    const newUser = {
      name: name,
      email: email,
      password: password
    }
    dispatch(authOperations.register(newUser));
    resetForm();
  }, [dispatch, name, email, password])

  const resetForm = () => {
    setName("");
    setEmail("");
    setPassword("");
  };

  return (
    <div className={styles.container}>
      <form
        className={themeLight ? styles.form : styles.formDark}
        onSubmit={handleSubmit}
        autoComplete="off"
      >
        <label className={styles.label}>
          {t("registerFormName")}
          <input
            className={styles.input}
            type="text"
            name="name"
            value={name}
            onChange={handleChange}
          />
        </label>
        <label className={styles.label}>
          {t("registerFormEmail")}
          <input
            className={styles.input}
            type="text"
            name="email"
            value={email}
            onChange={handleChange}
          />
        </label>
        <label className={styles.label}>
          {t("registerFormPassword")}
          <input
            className={styles.input}
            type="password"
            name="password"
            value={password}
            onChange={handleChange}
          />
        </label>
        {registrationError && (
          <p className={styles.error}>{registrationError}</p>
        )}
        <button
          className={themeLight ? styles.btn : styles.btnDark}
          type="submit"
        >
          {t("registerFormBtn")}
        </button>
      </form>
    </div>
  );
}

RegisterView.propTypes = {
  t: PropTypes.func,
};

export default withTranslation()(RegisterView);
