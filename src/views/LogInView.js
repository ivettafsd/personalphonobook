import { useState, useCallback } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { withTranslation } from 'react-i18next';
import PropTypes from 'prop-types';
import { getTheme } from '../redux/theme/theme-selectors';
import { authOperations, authSelectors } from '../redux/auth';
import styles from './RegisterView.module.css';

function LogInView({ t }) {
  // mapStateToProps
  const themeLight = useSelector(getTheme);
  const loginFailed = useSelector(authSelectors.getErrorMessage);

  //mapDispatchToProps
  const dispatch = useDispatch();

  //state
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
 
  const handleChange = useCallback((event) => {
    const { name, value } = event.currentTarget;

    switch (name) {
      case 'email':
        return setEmail(value);

      case 'password':
        return setPassword(value);

      default:
        return null;
    }
  }, [])
  
  const handleSubmit = useCallback((event) => {
    event.preventDefault();
    dispatch(authOperations.logIn({ email: email, password: password }))
    resetForm();
  }, [dispatch, email, password])

  const resetForm = () => {
    setEmail('');
    setPassword('')
  };

  return (
    <div className={styles.container}>
      <form
      className={themeLight ? styles.form : styles.formDark}
      onSubmit={handleSubmit}
      autoComplete="off"
      >
      <label className={styles.label}>
        {t("loginFormEmail")}
        <input
          className={styles.input}
          type="text"
          name="email"
          value={email}
          onChange={handleChange}
        />
      </label>
      <label className={styles.label}>
        {t("loginFormPassword")}
        <input
          className={styles.input}
          type="password"
          name="password"
          value={password}
          onChange={handleChange}
        />
      </label>
      {loginFailed && (
        <p className={styles.error}>{loginFailed}</p>
      )}
      <button
        className={themeLight ? styles.btn : styles.btnDark}
        type="submit"
      >
        {t("loginFormBtn")}
      </button>
    </form>
  </div>)
}

LogInView.propTypes = {
  t: PropTypes.func
}

export default withTranslation()(LogInView)

