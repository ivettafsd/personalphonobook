const routes = {
    home: '/',
    phonebook: '/personal-phonebook',
    register: '/register',
    login: '/login'
}
export default routes;