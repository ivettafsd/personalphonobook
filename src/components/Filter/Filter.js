import PropTypes from 'prop-types';
import { useSelector, useDispatch } from 'react-redux';
import * as actionsPhonebook from '../../redux/phonebook/phonebook-actions';
import { withTranslation } from 'react-i18next';
import { phonebookSelectors } from '../../redux/phonebook';
import styles from './Filter.module.css';

function Filter({ t }) {
  //mapStateToProps
  const value = useSelector(phonebookSelectors.getFilter);

  //mapDispatchToProps
  const dispatch = useDispatch();

  function handleChange({ currentTarget: { value } }){
    dispatch(actionsPhonebook.filter(value))
  }

  return (
    <div className={styles.filter}>
      <p className={styles.title}>{t("filter")}</p>
      <input
        type="text"
        name="filter"
        pattern="^[a-zA-Zа-яА-Я]+(([' -][a-zA-Zа-яА-Я ])?[a-zA-Zа-яА-Я]*)*$"
        title="Имя может состоять только из букв, апострофа, тире и пробелов. Например Adrian, Jacob Mercer, Charles de Batz de Castelmore d'Artagnan и т. п."
        required
        value={value}
        onChange={handleChange}
      />
    </div>
  );
};

Filter.propTypes = {
  t: PropTypes.func,
};

export default  withTranslation()(Filter);
