import { NavLink } from "react-router-dom";
import {useSelector } from "react-redux";
import { withTranslation } from "react-i18next";
import PropTypes from "prop-types";
import { getTheme } from "../../redux/theme/theme-selectors";
import routes from "../../routes";
import AuthNav from "../AuthNav";
import UserMenu from "../UserMenu";
import BtnTheme from "../BtnTheme";
import BtnLanguage from "../BtnLanguage";
import { authSelectors } from '../../redux/auth';
import styles from "./AppBar.module.css";

function AppBar({ t }) {
  //mapStateToProps
  const themeLight = useSelector(getTheme);
  const isAuthenticated = useSelector(authSelectors.getIsAuthenticated);

  return (
    <header className={styles.header}>
      <nav className={styles.box}>
        <NavLink
          exact
          to={routes.home}
          className={themeLight ? styles.link : styles.linkDark}
          activeClassName={styles.linkActive}
        >
          {t("homeView")}
        </NavLink>
        {isAuthenticated && (
          <>
          <NavLink
            exact
            to={routes.phonebook}
            className={themeLight ? styles.link : styles.linkDark}
            activeClassName={styles.linkActive}
          >
            {t("phonebookView")}
          </NavLink>
            </>
        )}
      </nav>
      <div className={styles.box}>
        {isAuthenticated ? <UserMenu /> : <AuthNav />}
        <BtnTheme />
        <BtnLanguage />
      </div>
    </header>
  );
};

AppBar.propTypes = {
  t: PropTypes.func,
};

export default withTranslation(["translation", "views"])(AppBar);
