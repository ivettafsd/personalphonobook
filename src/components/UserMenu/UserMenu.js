import { useSelector, useDispatch} from "react-redux";
import { withTranslation } from "react-i18next";
import { getTheme } from "../../redux/theme/theme-selectors";
import { authSelectors, authOperations } from '../../redux/auth';
import styles from "./UserMenu.module.css";
import authSprite from "./user-sprite.svg";
import avatar from "./avatar.png";

function UserMenu() {
  //mapStateToProps
  const themeLight = useSelector(getTheme);
  const name = useSelector(authSelectors.getUsersName);
  //mapDispatchToProps 
  const dispatch = useDispatch();

  return (
    <div className={styles.box}>
      <div className={styles.user}>
        <img className={styles.img} src={avatar} alt="Аватар пользователя" />
        <h3 className={themeLight ? styles.nameLight : styles.nameDark}>{name}</h3>
      </div>
      <button
        className={themeLight ? styles.btnLight : styles.btnDark}
        onClick={() => dispatch(authOperations.logOut())}
      >
        {themeLight ? (
          <svg className={styles.iconLight} width="20" height="20">
            <use href={`${authSprite}#icon-exit`}></use>
          </svg>
        ) : (
          <svg className={styles.iconDark} width="20" height="20">
            <use href={`${authSprite}#icon-exit`}></use>
          </svg>
        )}
      </button>
    </div>
  );
};

export default withTranslation()(UserMenu);
