import { Suspense, lazy, useEffect } from "react";
import { Switch, Route } from "react-router-dom";
import Loader from "react-loader-spinner";
import { useSelector, useDispatch } from "react-redux";
import { getTheme } from "../../redux/theme/theme-selectors";
import AppBar from "../AppBar";
import PrivateRoute from "../PrivateRoute";
import PublicRoute from "../PublicRoute";
import { authOperations } from "../../redux/auth";
import routes from "../../routes";
import styles from "./App.module.css";

const HomeView = lazy(() =>
  import("../../views/HomeView" /* webpackChunkName: "home-view" */)
);
const LogInView = lazy(() =>
  import("../../views/LogInView" /* webpackChunkName: "login-view" */)
);
const PhonebookView = lazy(() =>
  import("../../views/PhonebookView" /* webpackChunkName: "phonebook-view" */)
);
const RegisterView = lazy(() =>
  import("../../views/RegisterView" /* webpackChunkName: "register-view" */)
);
const NotFoundView = lazy(() =>
  import("../../views/NotFoundView" /* webpackChunkName: "notFound-view" */)
);

function App() {
  //mapStateToProps
  const themeLight = useSelector(getTheme);

  //mapDispatchToProps
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(authOperations.getCurrentUser());
  }, [dispatch]);

  return (
    <div className={themeLight ? styles.bodyLight : styles.bodyDark}>
      <AppBar />
      <Suspense
        fallback={<Loader type="Hearts" color="gray" height={80} width={80} />}
      >
        <Switch>
          <PublicRoute
            path={routes.register}
            restricted
            redirectTo={routes.home}
          >
            <RegisterView />
          </PublicRoute>

          <PublicRoute
            path={routes.login}
            restricted
            redirectTo={routes.home}
          >
            <LogInView />
          </PublicRoute>

          <PublicRoute exact path={routes.home}>
            <HomeView />
          </PublicRoute>

          <PrivateRoute path={routes.phonebook} redirectTo={routes.login}>
            <PhonebookView />
          </PrivateRoute>

          <Route component={NotFoundView} />
        </Switch>
      </Suspense>
    </div>
  );
}

export default App;
