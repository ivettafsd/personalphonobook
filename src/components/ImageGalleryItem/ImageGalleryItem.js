import PropTypes from 'prop-types';
import defaultImg from './default.svg';

const ImageGalleryItem = ({ src, alt, dataUrl }) => (
  <>
    <img
      src={src}
      alt={alt}
      data-url={dataUrl}
      width="320"
      height="200"
    />
  </>
);

ImageGalleryItem.defaultProps = {
  alt: 'Фото',
  webformatURL: defaultImg,
  largeImageURL: defaultImg,
};

ImageGalleryItem.propTypes = {
  src: PropTypes.string,
  alt: PropTypes.string,
  dataUrl: PropTypes.string,
};

export default ImageGalleryItem;
