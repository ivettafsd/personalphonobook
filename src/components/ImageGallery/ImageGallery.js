import PropTypes from 'prop-types';
import ImageGalleryItem from '../ImageGalleryItem';
import styles from './ImageGallery.module.css';

const ImageGallery = ({ list, onClick }) => (
  <ul className={styles.list} onClick={onClick}>
    {list.map(item => (
      <li className={styles.item} key={item.id}>
        <ImageGalleryItem
          src={item.webformatURL}
          alt={item.tags}
          dataUrl={item.largeImageURL}
        />
      </li>
    ))}
  </ul>
);

ImageGallery.propTypes = {
  list: PropTypes.arrayOf(PropTypes.object).isRequired,
  onClick: PropTypes.func.isRequired,
};

export default ImageGallery;
