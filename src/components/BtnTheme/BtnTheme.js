import { useSelector, useDispatch } from 'react-redux';
import * as actionsTheme from '../../redux/theme/theme-actions';
import { getTheme } from '../../redux/theme/theme-selectors';
import themeSprite from "./theme-sprite.svg";
import styles from './BtnTheme.module.css';

function BtnTheme() {
  //mapToStateProps
  const themeLight = useSelector(getTheme);
  //mapDispatchToProps
  const dispatch = useDispatch();

  function handleClick (){
    dispatch(actionsTheme.changeTheme())
  }

  return (
    <button
      className={themeLight ? styles.btnLight : styles.btnDark}
      onClick={handleClick}
    >
      {themeLight ? (
        <svg className={styles.iconLight} width="20" height="20">
          <use href={`${themeSprite}#icon-night`}></use>
        </svg>
      ) : (
        <svg className={styles.iconDark} width="20" height="20">
          <use href={`${themeSprite}#icon-light`}></use>
        </svg>
      )}
    </button>
  );
};

export default BtnTheme;
