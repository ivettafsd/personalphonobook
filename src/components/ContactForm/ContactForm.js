import { useState, useCallback } from "react";
import { useSelector, useDispatch } from "react-redux";
import { phonebookOperations } from "../../redux/phonebook";
import { withTranslation } from "react-i18next";
import PropTypes from "prop-types";
import { getTheme } from "../../redux/theme/theme-selectors";
import styles from "./ContactForm.module.css";

function ContactForm({ t }) {
  //mapStateToProps
  const themeLight = useSelector(getTheme);

  //mapDispatchToProps
  const dispatch = useDispatch();

  //state
  const [name, setName] = useState("");
  const [number, setNumber] = useState("");

 const handleNameChange = useCallback(event => {
    const { name, value } = event.currentTarget;
    switch (name) {
      case "name":
        return setName(value);

      case "number":
        return setNumber(value);

      default:
        return null;
    }
  }, [])

  const handleSubmit = useCallback(event => {
    event.preventDefault();
    const newContact = { name, number };
    dispatch(phonebookOperations.addContact(newContact));
    resetForm();
  }, [dispatch, name, number])

  function resetForm() {
    setName("");
    setNumber("");
  }

  return (
    <form
      className={themeLight ? styles.form : styles.formDark}
      onSubmit={handleSubmit}
    >
      <label className={styles.label}>
        {t("contactFormName")}
        <input
          className={styles.input}
          type="text"
          name="name"
          pattern="^[a-zA-Zа-яА-Я]+(([' -][a-zA-Zа-яА-Я ])?[a-zA-Zа-яА-Я]*)*$"
          title="Имя может состоять только из букв, апострофа, тире и пробелов. Например Adrian, Jacob Mercer, Charles de Batz de Castelmore d'Artagnan и т. п."
          required
          value={name}
          onChange={handleNameChange}
        />
      </label>
      <label className={styles.label}>
        {t("contactFormNumber")}
        <input
          className={styles.input}
          type="tel"
          name="number"
          pattern="(\+?( |-|\.)?\d{1,2}( |-|\.)?)?(\(?\d{3}\)?|\d{3})( |-|\.)?(\d{3}( |-|\.)?\d{4})"
          title="Номер телефона должен состоять из 11-12 цифр и может содержать цифры, пробелы, тире, пузатые скобки и может начинаться с +"
          required
          value={number}
          onChange={handleNameChange}
        />
      </label>
      <button
        className={themeLight ? styles.btn : styles.btnDark}
        type="submit"
      >
        {t("contactFormBtn")}
      </button>
    </form>
  );
}

ContactForm.propTypes = {
  t: PropTypes.func,
};

export default withTranslation()(ContactForm);
