import { useSelector} from "react-redux";
import { withTranslation } from "react-i18next";
import PropTypes from "prop-types";
import { getTheme } from "../../redux/theme/theme-selectors";
import styles from "./BtnLanguage.module.css";

function BtnLanguage({ i18n }) {
  const themeLight = useSelector(getTheme);

  function handleClick(event) {
    i18n.changeLanguage(event.target.name);
  }

  return (
    <div onClick={handleClick}>
      {i18n.language === "ru" ? (
        <button
          name="en"
          className={themeLight ? styles.btnLight : styles.btnDark}
        >
          EN
        </button>
      ) : (
        <button
          name="ru"
          className={themeLight ? styles.btnLight : styles.btnDark}
        >
          RU
        </button>
      )}
    </div>
  );
};

BtnLanguage.propTypes = {
  i18n: PropTypes.object.isRequired,
};

export default withTranslation()(BtnLanguage);
