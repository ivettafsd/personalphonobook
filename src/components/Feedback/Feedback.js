import { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import { withTranslation } from "react-i18next";
import { getTheme } from "../../redux/theme/theme-selectors";
import styles from './Feedback.module.css';

function Feedback({ t }) {
    const themeLight = useSelector(getTheme);

  const [good, setGood] = useState(0);
  const [neutral, setNeutral] = useState(0);
  const [bad, setBad] = useState(0);
  const [positiveFeedback, setPositiveFeedback] = useState(0);

  useEffect(() => {
    setPositiveFeedback(Math.round(
      (good * 100) / (good + neutral + bad),
    ))
  }, [good, neutral, bad]);

  const handleClick = ({ target: { name } }) => {
    switch (name) {
      case "good":
        return setGood(good + 1);
      case "neutral":
        return setNeutral(neutral + 1);
      case "bad":
        return setBad(bad + 1);
      default:
        return null;
    }
  };

  return (
    <div onClick={handleClick}>
      <div>
        <button
          className={themeLight ? styles.btnLight : styles.btnDark}
          name="good"
        >
          {t("feedbackGood")}
        </button>
        <button
          className={themeLight ? styles.btnLight : styles.btnDark}
          name="neutral"
        >
          {t("feedbackNeutral")}
        </button>
        <button
          className={themeLight ? styles.btnLight : styles.btnDark}
          name="bad"
        >
          {t("feedbackBad")}
        </button>
      </div>
      <div className={themeLight ? styles.statisticsLight : styles.statisticsDark}>
        <h3 className={themeLight ? styles.textLight : styles.textDark}>
          {t("statistics")}
        </h3>
        <ul>
          <li className={themeLight ? styles.textLight : styles.textDark}>
            {t("feedbackGood")}: {good}
          </li>
          <li className={themeLight ? styles.textLight : styles.textDark}>
            {t("feedbackNeutral")}: {neutral}
          </li>
          <li className={themeLight ? styles.textLight : styles.textDark}>
            {t("feedbackBad")}: {bad}
          </li>
        </ul>
        {positiveFeedback ? (
          <h3 className={themeLight ? styles.textLight : styles.textDark}>
            {t("feedbackTotal")}: {positiveFeedback}%
          </h3>
        ) : null}
      </div>
    </div>
  );
}
export default withTranslation()(Feedback);
