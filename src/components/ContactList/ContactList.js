import { useSelector, useDispatch } from "react-redux";
import { phonebookOperations, phonebookSelectors } from "../../redux/phonebook";
import { getTheme } from "../../redux/theme/theme-selectors";
import deleteImg from "./delete.png";
import styles from "./ContactList.module.css";

function ContactList() {
  //mapStateToProps
  const themeLight = useSelector(getTheme);
  const contacts = useSelector(phonebookSelectors.getVisibleContacts);

  //mapDispatchToProps
  const dispatch = useDispatch();

  return (
    <ul className={styles.list}>
      {contacts.map((item) => (
        <li
          className={themeLight ? styles.item : styles.itemDark}
          key={item.id}
        >
          <span className={themeLight ? styles.name : styles.nameDark}>
            {item.name}
          </span>
          <span>{item.number}</span>
          <button className={styles.btn} onClick={() => dispatch(phonebookOperations.delContact(item.id))}>
            {" "}
            <img className={styles.img} src={deleteImg} alt="" width="10" />
          </button>
        </li>
      ))}
    </ul>
  );
};

export default ContactList;
