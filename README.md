### Instructions for launching the application

1. You can learn the application to click on https://ivetta-personalphonobook.netlify.app/
2. If you want to deploy the application on your computer you need to do following:
  - to clone it from the repository https://gitlab.com/ivettafsd/personalphonobook
  - to copy the link for the cloning https://gitlab.com/ivettafsd/personalphonobook.git
  - to open the terminal on your computer
  - to go to the nessesary folder (for example, terminal's command 'cd documents/...'
  - to enter the following commands
    'git clone https://gitlab.com/ivettafsd/personalphonobook.git'
    'cd personalphonobook'
    'npm install'
    'npm start'
3. The application is deploed and you can learn it

